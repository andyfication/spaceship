//
//  Menu.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 26/01/2016.
//
//

#ifndef Menu_hpp
#define Menu_hpp


#include "ofMain.h"
#include "Aliens.hpp"
#include "Ship.hpp"
#include "Health_Bar.hpp"
#include "Enemy.hpp"
#include"Score.hpp"
#include"GameObject.hpp"



//------------------- Class Main Menu ---------------------------
class Menu:public GameObject
{
public:
    
    ofVec2f position;
    ofVec2f arrow_position;
    ofVec2f text_position;
    float astx,asty,aspeex,aspeedy,size;// steroids main menu animation variables
    int astIndex; // index asteroid displayed
    string a,b,c; // menu options text string
    ofTrueTypeFont font; // font main menu
    ofTrueTypeFont font1; // other font
    vector <ofImage> images; // arrow animation images
    bool right; // used with the arrow animation
    bool alien_active; // boolean used to see if the alien animation is active
    Alien* aln; // reference to the class Alien
    Ship* ship; // reference to the class Ship
    Health* h; // reference to health bar
    vector<unique_ptr<Enemy>>& en;
    vector <unique_ptr<Alien>> &al;
    bool load_alienVect;
    Score *s;
    
    
    //------------- Menu Design--------------------------------------
   
    bool sel = true; // checking which menu option is selected
    string gameState; // used to check the game state section
    
    
public:
    Menu(float _x, float _y,string _a,string _b,string _c,Alien* _aln,Ship* _ship,Health* _h,vector<unique_ptr<Enemy>> &_en, vector <unique_ptr<Alien>> &_al,Score * _s); // constructor position,three string text
    virtual~Menu();//disctructor 
    void setup_menu_images();//Load images and text font
    void displayStart(); // arrows on start
    virtual void play(); // play section
    void displayHow(); // arrows on how to play
    void displayBack(); // back button on how to play section
    void displayGameover(); // game over menu design
    void condition(int key); // different stages based on key pressed
    bool select(); // selected stage of the game
    void instructions(); // game instructions
    virtual void update(); // update arrow animation in the menu
    void randomAsteroids(); // main menu asteroids display
    void transaction(); //just before start he game
    void transaction_play(); // transaction section 
    void animation(); // animation section
    void update_time();// used for the start ship animation
    
    
    
    
    
    
    
    
    
};

#endif /* Menu_hpp */
