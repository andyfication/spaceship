#pragma once

#include "ofMain.h"
#include "menu.hpp"
#include "Ship.hpp"
#include "Ship_Confing.hpp"
#include "Aliens.hpp"
#include "Enemy.hpp"
#include"Target.hpp"
#include"math.h"
#include"Health_Bar.hpp"
#include"Score.hpp"
#include "Bullets.hpp"
#include "Power.hpp"


class ofApp : public ofBaseApp{

	public:
    bool isPressed; // key pressed?
    bool isOsc; // using osc?
    int currentKey; // which key is pressed
    ofImage bas; // image for basic asteroid
    ofImage med; // image for medium asteroid
    ofImage adv; // image for advanced asteroid
    vector <ofImage> explosion; // used for explosion animation
    int current_exp; // display current explosion
    bool display_explosion; // hide/display explosion
    bool display_explosion_moon;// explosion for the moon collision
    void setup();
    void update();
    void draw();
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    void explosion_an();
    void random_asteroids(); // randomise asteorids
    Menu* menu; // object of type Menu
    Ship* ship1; // ship object
    Alien* alien_animation; // alien used for the main menu animation
    Target* target; // object of type target
    Health* health_left; // helath object
    Health* health_background; // helath object for the live bar
    Score* collectable; // object collectable
    
    
    vector <unique_ptr<Power>>powers; // unique pointers aliens for the game display
    vector <unique_ptr<Alien>> aliens; // unique pointers aliens for the game display
    vector<unique_ptr<Enemy>> enemies; // unique pointers of base class Enemy which will hold subclasses for polymorphisms

    
    
 
    vector<unique_ptr<GameObject>> gameobjects; // container for gameobjecs 
 

        
        


    
		
};
