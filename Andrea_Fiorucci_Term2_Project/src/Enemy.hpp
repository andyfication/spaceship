//
//  Enemy.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 27/02/2016.
//
//

#ifndef Enemy_hpp
#define Enemy_hpp

#include <stdio.h>
#include "ofMain.h"
#include "Ship.hpp"
#include"Target.hpp"
#include"Health_Bar.hpp"
#include"GameObject.hpp"


// Base_Class Enemy used as abstract class for inheritance
class Enemy : public GameObject
{
protected:
    ofVec2f location; // position
    ofVec2f velocity; // velocity
    ofVec2f acceleration; // acceleration
    float max_speed; // limit velocity
    ofImage design; // image
    float radius; // radius for collision
    int strenght; // how strong is each asteroid
    Ship* sh; // reference to ship
    Health* hl; // reference to health bar
    
  
   
public:
    
Enemy(float x,float y,float _max_speed,ofImage _design,int _strenght,int _radius,Ship* _sh,Health* _hl); // constructor
    virtual ~Enemy(); // distructor declared as virtual so that objects are destroied in the correct order
    virtual void update(); // update used only for the basic_asteroids
    virtual void display()=0; // pure virtual function (sub-classes are displayed differently)
    virtual bool collision_player(); // check for collision with player (sub_classes have different size)
    virtual bool collision_moon();   // check for collision with moon (sub_classes have different size)
    virtual float getLocationX(); // public location x
    virtual float getLocationY(); // public location y
    virtual float getRadius(); // get public radius
    virtual int getStrenght(); // get public strenght
    virtual void setStrenght(int s); // set strenght
    virtual bool destroyed(); // check for enemy destroyed
    
};


// ----------------------------------------------------------basic_asteroid which inherits from Enemy class
class Basic_Asteroid:public Enemy
{
 
public:
    Basic_Asteroid(float x,float y,float _max_speed,ofImage _design,int _strenght,int _radius,Ship* _sh,Health* _hl); // initialiser list to import the constructor
    ~Basic_Asteroid(); // distructor
    virtual void display(); // display differently
    virtual bool collision_player(); // player collision different from base class
    virtual bool collision_moon(); // mooon collision different from base class
    virtual float getLocationX(); // public location x
    virtual float getLocationY(); // public location y
    virtual float getRadius(); // get public radius
    virtual int getStrenght(); // get public strenght
    virtual void setStrenght(int s); // set strenght
    virtual bool destroyed(); // check for enemy destroyed
};

// -----------------------------------------------------------medium_asteroid which inherits from Enemy class
class Medium_Asteroid:public Enemy
{
public:
    Medium_Asteroid(float x,float y,float _max_speed,ofImage _design,int _strenght,int _radius,Ship* _sh,Health* _hl);// initialiser list to import the constructor
    ~Medium_Asteroid();//distructor
    virtual void display(); // display differently
    virtual bool collision_player(); // player collision different from base class
    virtual bool collision_moon(); // mooon collision different from base class
    virtual void update(); // updated differently
    virtual float getLocationX(); // public location x
    virtual float getLocationY(); // public location y
    virtual float getRadius(); // get public radius
    virtual int getStrenght(); // get public strenght
    virtual void setStrenght(int s); // set strenght
    virtual bool destroyed(); // check for enemy destroyed
};

//--------------------------------------------------------------- advanced_asteroid which inherits from Enemy class
class Advanced_Asteroid:public Enemy
{
private:
    Target* target; // sub_class extension by adding a new member variable (reference to the target class)
public:
    Advanced_Asteroid(float x,float y,float _max_speed,ofImage _design,int _strenght,int _radius,Ship* _sh,Target* _target,Health* _hl);// initialiser list to import the constructor and added a memenr variable of this sub-class
    ~Advanced_Asteroid();//distructor
    virtual void display(); // display differently
    virtual bool collision_player(); // player collision different from base class
    virtual bool collision_moon(); // mooon collision different from base class
    virtual void update(); // updated differently
    virtual float getLocationX(); // public location x
    virtual float getLocationY(); // public location y
    virtual float getRadius(); // get public radius
    virtual int getStrenght(); // get public strenght
    virtual void setStrenght(int s); // set strenght
    virtual bool destroyed(); // check for enemy destroyed
};

#endif /* Enemy_hpp */
