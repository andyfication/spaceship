//
//  Menu.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 26/01/2016.
//
//

#include "Menu.hpp"
// takes in position x and y, three strings for displaying text and two references to ship and aliens objects
Menu::Menu(float _x, float _y,string _a,string _b,string _c,Alien* _aln,Ship* _ship,Health* _h,vector<unique_ptr<Enemy>>& _en, vector <unique_ptr<Alien>> &_al,Score* _s):GameObject(_x,_y),a(_a),b(_b),c(_c),aln(_aln),ship(_ship),
h(_h),en(_en),al(_al),s(_s)
{
    position.x = _x; // staring position of the arrows
    position.y = _y;
    arrow_position.x = _x; // starting position of the arrows
    text_position.x = _x;
    right = true; // arrow start moving right
    astx = ofGetScreenWidth()/2;
    asty = 200; // ast y position
    aspeex = 2; // speed x
    aspeedy = 5; // speed y
    astIndex = 5; // index
    size = 200; // size
    alien_active = aln->getActive(); // getting the boolean value from alien class
    load_alienVect = true;
    
  
}

Menu::~Menu()//disctructor 
{
    
}

//------Load images and text font
void Menu::setup_menu_images()
{
    // loading images inside a vector
    for (int i = 0; i<9;i++)
    {
        ofImage temp;
        string text = to_string(i);
        temp.loadImage(text+".png");
        images.push_back(temp);
    }
    
    font.load("font.tff",40); // loading fonts
    font1.load("font.tff",25);
    gameState = "menu"; // game start from the main menu stage
    
    
    
}


// main menu randomise asteroids displayed
void Menu::randomAsteroids()
{
    if(asty>ofGetScreenHeight()+100) // if off screen randomise another one
    {
        astx = ofGetScreenWidth()/2-300;
        asty = -200;
        aspeex = ofRandom(-10, 10);
        aspeedy =ofRandom(3, 6);
        astIndex = ofRandom(5, 8);
        size = ofRandom(100,250);
    }
    
    astx+=aspeex; // update speed x
    asty+=aspeedy;// update speed y
}
//---------- Main Menu-------------------
void Menu::displayStart()
{
    // menu design
    images[1].draw(0,0,1200,900);
    images[astIndex].draw(astx,asty,size,size);
    images[4].draw(170,30);
    images[0].draw(-395,position.y+150);
    images[3].draw((position.x-55),position.y-91,70,40);
    images[2].draw(arrow_position.x+178,position.y-91,70,40);
    font.drawString(a, text_position.x+30, position.y-50);
    font.drawString(b, text_position.x-57, position.y+100);
    
    
}


void Menu::update_time() // used in the transacion play state for a quick animation before the starting point
{
    float temp_time = ship->get_time();
    temp_time-=0.1;
    ship->set_time(temp_time);
    
    
}

// menu fades to the top
void Menu::transaction()
{
    // menu design
    images[1].draw(0,0,1200,900);
    images[4].draw(170,position.y-370);
    images[0].draw(-395,550);
    images[3].draw((position.x-55),position.y-91,70,40);
    images[2].draw(arrow_position.x+178,position.y-91,70,40);
    font.drawString(a, text_position.x+30, position.y-50);
    font.drawString(b, text_position.x-57, position.y+100);
    position.y-=10;
    if(position.y<-110) // when menu reaches the top the next game section is called
    {
        gameState = "animation";
        position.y = 400; // bring back the menu to position
        
    }
    
    
}

// section just before play
void Menu::animation()
{
    
    if (gameState == "animation" )
    {
        
        if(aln->getMax()<0) // when the alien enter the ship start play
        {
            gameState = "play";
            s->setReset(true);
            
            
        }
        
    }
}




void Menu::play() // play section background
{
    
    images[1].draw(0,0,1200,900);
    images[0].draw(-395,position.y+150);
   
   
    
}



// animating the arrow to make the menu more realistic ( arrow movement left and right)
void Menu::update()
{
   
   if (position.x>500)
    {
        right = false;
    }
    else if (position.x<480)
    {
       right = true;
    }
    
    if (right)
    {
        position.x++;
        arrow_position.x--;
    }
    else
    {
        position.x--;
        arrow_position.x++;

    }
    
    
 
    
 
    
    
    
}
//---------- Main Menu with arrows pointing to the section "how to play"
void Menu::displayHow()
{
    images[1].draw(0,0,1200,900);
    images[astIndex].draw(astx,asty,size,size);
    images[4].draw(170,30);
    images[0].draw(-395,position.y+150);
    images[3].draw(position.x-145,position.y+60,70,40);
    images[2].draw(arrow_position.x+270,position.y+60,70,40);
    font.drawString(a, text_position.x+30, position.y-50);
    font.drawString(b, text_position.x-57, position.y+100);
    
}
//---------- How to play section-----------------------
void Menu::displayBack()
{
    images[1].draw(0,0,1200,900);
    images[astIndex].draw(astx,asty,size,size);
    images[4].draw(170,30);
    images[0].draw(-395,position.y+150);
    images[3].draw(position.x-50,position.y+210,70,40);
    images[2].draw(arrow_position.x+177,position.y+210,70,40);
    font.drawString(c, text_position.x+35, position.y+250);
}
//---------- display the menu game over stage-------------
void Menu::displayGameover()
{
   
    string temps;
    string temph;
    temps = to_string(s->getScore());
    temph = to_string(s->getHigh());
    images[1].draw(0,0,1200,900);
    images[astIndex].draw(astx,asty,size,size);
    images[4].draw(170,30);
    ofPushStyle();
    ofSetColor(100, 20, 0,255);
    ofFill();
    images[0].draw(-395,position.y+150);
    ofPopStyle();
    images[3].draw(position.x-50,position.y+210,70,40);
    images[2].draw(arrow_position.x+177,position.y+210,70,40);
    font.drawString("Menu", text_position.x+24, position.y+250);
    font.drawString("Planet Aly has been destroyed", 200, 250);
    ofPushStyle();
    ofSetColor(0, 150, 150,150);
    ofFill();
    font1.drawString("Score:  "+ temps, 200, 550);
    ofPopStyle();
    ofPushStyle();
    ofSetColor(255, 180, 100,100);
    ofFill();
    font1.drawString("HighScore:  "+temph, ofGetWidth()-400, 550);
    ofPopStyle();
    ofPushStyle();
    ofSetColor(155, 155, 0,150);
    ofFill();
    string temp = "   m: " +to_string(s->getMinutes())+"  s: " +to_string(s->getSeconds());
    font1.drawString("Time Survived "+temp, ofGetWidth()/2-230, 400);
     ofPopStyle();
}


//---------- display the menu instructions  stage---------------
void Menu :: instructions()
{
    
    images[8].draw(250,170);
    
}

//-------------------------- menu logic to change among different stages
void Menu::condition(int key)

{
    
    if(key == OF_KEY_UP && gameState == "menu")
    {
        sel = true; // aniamtion boolean for the menu
    }
    else if(key == OF_KEY_DOWN && gameState == "menu")
    {
        sel = false; // aniamtion boolean for the menu
    }
    
    else if (key == OF_KEY_RETURN && select() ==true && gameState == "menu")
    {
        
        gameState ="transaction"; // change the state if press enter
        
        
    }
    else if (gameState == "animation" && key)
    {
        //reset values
       alien_active = true;
       aln->setActive(alien_active);
        
    }
   
    else if (key == OF_KEY_RETURN && select() ==false && gameState == "menu")
    {
        gameState ="how"; // change the state if press enter
       
    }
    else if (key == OF_KEY_BACKSPACE && (gameState == "play"))
    {
        // reset values
        gameState ="menu";
        aln->setMax(6);
        alien_active = false;
        aln->setActive(alien_active);
        bool tmp = aln->getSee();
        tmp = true;
        aln->setSee(tmp);
        aln->setX(100);
        aln->setY(ofGetHeight()-50);
        ofVec2f tempLoc;
        tempLoc.set(ofGetWidth()/2,ofGetHeight()-60);
        ship->setLocation(tempLoc);
        ship->setDir(0);
        ofVec2f acc;
        acc = ship->getAcceleration();
        acc.x = 0;
        acc.y = 0;
        ship->setAcceleration(acc);
        ofVec2f f;
        f = ship->geForce();
        f.x = 0;
        f.y = 0;
        ship->setForce(f);
        ofVec2f v;
        v = ship->getVelociy();
        v.x = 0;
        v.y = 0;
        ship->setVelocity(v);
        ship->set_time(3);
        aln->set_alien_left(6);
        h->set_h(500);
        h->set_y(250);
        h->set_increment(0);
        
       
        for(auto &e: en)
        {
            en.clear();
        }
        
        for(auto &a: al)
        {
            al.clear();
        }
        for(auto &b: ship->bl)
        {
            ship->bl.clear();
        }

        load_alienVect = true;
        s->setScore(0);
        s->setSeconds(0);
        s->setMinutes(0);
        
    }
    else if (key == OF_KEY_RETURN && gameState == "how")
    {
        gameState ="menu"; // change the state if press esc
    }
    
    
    else if (key == OF_KEY_RETURN && gameState == "lose")
    {
        //reset values 
        gameState ="menu"; // change the state if press esc
        aln->setMax(6);
        alien_active = false;
        aln->setActive(alien_active);
        bool tmp = aln->getSee();
        tmp = true;
        aln->setSee(tmp);
        aln->setX(100);
        aln->setY(ofGetHeight()-50);
        ofVec2f tempLoc;
        tempLoc.set(ofGetWidth()/2,ofGetHeight()-60);
        ship->setLocation(tempLoc);
        ship->setDir(0);
        ofVec2f acc;
        acc = ship->getAcceleration();
        acc.x = 0;
        acc.y = 0;
        ship->setAcceleration(acc);
        ofVec2f f;
        f = ship->geForce();
        f.x = 0;
        f.y = 0;
        ship->setForce(f);
        ofVec2f v;
        v = ship->getVelociy();
        v.x = 0;
        v.y = 0;
        ship->setVelocity(v);
        ship->set_time(3);
        aln->set_alien_left(6);
        h->set_h(500);
        h->set_y(250);
        h->set_increment(0);
        
        for(auto &e: en)
        {
            en.clear();
        }
        
        for(auto &b: ship->bl)
        {
            ship->bl.clear();
        }

        
        for(auto &a: al)
        {
            al.clear();
        }
        
        load_alienVect = true;
        s->setScore(0);

        
    }
    
   
    
    
}

//-------------------- returning true is bool sel = true, used to animate the menu
bool Menu::select()
{
    if (sel == true)
    {
        return true;
    }
    else if(sel == false)
    {
        return false;
    }
}