//
//  GameObject.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 14/03/2016.
//
//

#ifndef GameObject_hpp
#define GameObject_hpp

#include <stdio.h>
#include "ofMain.h"

// class game-objetc used as a base class for almost all the game objects
class GameObject
{
    
    ofVec2f position; // each object has a position
    
public:
    GameObject(float _x,float _y); // constructor with position
    virtual~GameObject(); // distructor 
    virtual void display(); // each object has a display function
    virtual void update(); // each object has a update function
    
};

#endif /* GameObject_hpp */
