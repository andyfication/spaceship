//
//  Score.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 04/03/2016.
//
//

#ifndef Score_hpp
#define Score_hpp

#include "ofMain.h"
#include "Ship.hpp"

// class score to keep track of score and display collectables
class Score:public GameObject
{
    ofVec2f position;
    ofTrueTypeFont sc; // fonts used to display score and highscore
    ofTrueTypeFont sc1;
    ofImage im; // image for the score star
    Ship * sh; // reference to ship object
    float sx; // animate collectable
    bool scale; // animate collectable
    int score; // score variable
    int highscore; // highscore variable
    int milliseconds; // used for time survived
    int high_millis;// used for time survived best time
    int minutes;// used for time survived
    int seconds;// used for time survived
    bool reset;// reset time
    
    
public:
    Score(float _x, float _y,Ship * _sh); // constructor
    virtual ~Score();//disctructor 
    void load_image(); // load images for score
    virtual void display(); // display the score
    virtual void update(); // randomise once picked
    bool collected(); // check if collected
    void setScore(int s);// set score
    void setHigh(int h); // set highscore
    void displayHigh(); // display highscore
    int getScore(); // get score
    int getHigh(); // get highscore
    void updateTime(); // start time and reset it
    void displayTime(); // display the time on screen
    void setSeconds(int s); // set seconds
    void setMinutes(int m); // set minutes
    int getMinutes(); // get minutes
    int getSeconds(); // get seconds
    void checkHigh(); // check highscore if there is one
    void setReset(bool r); // reset time 
    
};

#endif /* Score_hpp */
