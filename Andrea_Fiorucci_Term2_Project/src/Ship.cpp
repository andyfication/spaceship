//
//  Ship.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 09/02/2016.
//
//

#include "Ship.hpp"


// Ship cpnstructor which takes in the starting ship position
Ship::Ship(float _x, float _y,float lim,float imp,string a, vector<unique_ptr<Enemy>>& _enem):GameObject(_x,_y),enem(_enem)
{
    label = a;
    conf = new Ship_Config(lim,imp);
    location.set(_x,_y); // ship initial locatino
    velocity.set(0,0); // not moving at the beginning
    acceleration.set(0,0); // no force applied to the velocity
    dir = 0; // not rotating
    limit = conf->get_Limit();
    current_image = 0; // image displayed at the beginning for the engine sprite
    receive.setup(PORT); // set up a port for the OSC receiver (PORT 12345)
    left = false; // not rotating left
    right = false; // not rotating right
    up = false; // not moving with keys
    up1 = false; // not moving with another device
    counter = 0; // accelerometer number of angles to consider
    smooth_angle = 0; // start from 0 and then added to the rotation
    amount_smooth = 1500; // nice smoothing filter for the accelerometer
    impulse = conf->get_Impulse(); // impulse = impulse from the configuration class
    time = 3; // set a time for the animation
    radius = 50; // radius for collision = 50
    id = 0;//bullet id
     shot = true;
    
    // loading the vector with values ; 0 for now and used for the OSC controls accelerometer
    for(int i = 0; i<amount_smooth;i++)
    {
        angle.push_back(0);
    }
    
    cout << "OBJECT SHIP CREATED"<<endl;
    
}

// copy constructor used if I want to create a second player which is equal to the first (objects are not created both)
Ship::Ship(const Ship& first):enem(enem),GameObject(*this)
{
    cout << "COPY CONSTRUCTOR"<< this->label<<this<<endl;
    label = first.label;
    location = first.location;
    velocity = first.velocity;
    acceleration = first.acceleration;
    dir = first.dir;
    limit = first.limit;
    current_image= first.current_image;
    receive = first.receive;
    left = first.left;
    right = first.right;
    up = first.up;
    up1 = first.up1;
    counter = first.counter;
    smooth_angle = first.smooth_angle;
    amount_smooth = first.amount_smooth;
    impulse = first.impulse;
    id = first.id;
    conf = new Ship_Config(first.limit,first.impulse); // assign new configuration so that they dont share the same one
    copy(first.conf,first.conf,conf); // copy conf in first configuration
    
    
}

// assignment operator (similar to the copy constructor but the objects are both cretaed before the assignment)
Ship& Ship::operator=(const Ship& first)
{
    cout << "OVERLOADED ASSIGMENT CALLED"<< endl;
    // check for self assignment
    // ship1 = ship2;
    if (this == &first)
    {
        return *this;
    }
    label = first.label;
    location = first.location;
    velocity = first.velocity;
    acceleration = first.acceleration;
    dir = first.dir;
    limit = first.limit;
    current_image= first.current_image;
    receive = first.receive;
    left = first.left;
    right = first.right;
    up = first.up;
    up1 = first.up1;
    counter = first.counter;
    smooth_angle = first.smooth_angle;
    amount_smooth = first.amount_smooth;
    impulse = first.impulse;
    copy(first.conf,first.conf,conf);
    id = first.id;
    return *this; // return a value
    
    
}


Ship::~Ship() // distructor and destry allocated memory for configuration
{
    cout << "OBJECT SHIP DESTRUCTED"<< this->label<<this<<endl;
    delete conf ;
    conf = nullptr;
}


// loading all images/text and audio sources 
void Ship::player_images()
{
    
        for (int i = 0; i<6;i++)
    {
        ofImage temp;
        string text = to_string(0);
        string text1 = to_string(0);
        string text2 = to_string(i);
        temp.loadImage(text+text1+text2+".png");
        engine.push_back(temp);
        
    }
    
    
    conf->set_image_ship();
    img = conf->get_Image();
}


// physics basic prociples;
void Ship::update()
{
    velocity = velocity+acceleration; // velocity depends on acceleration changes
    velocity.limit(limit); // limit the velocity to a value
    location = location + velocity; // update the position
    acceleration = acceleration * 0; // reset the acceleration to make the ship moving correctly
    
}


// increase or decrease the ship rotation based on key_pressed
void Ship::face(string direction)
{
    if (direction == "right")  {
        dir= dir+0.05;
        if(dir>=6.55)
        {
            dir = 0;
        }
    }
    else if (direction =="left") {
        dir-=0.05;
        if(dir<=-6.55)
        {
            dir = 0;
        }
    }
    else if (direction =="straight") {
        dir-=0;
    }
    
    
}

// based on the rotation create a force pvector which holds the correct rotation.
void Ship::getForce() // direction when accelerate
{
    float angle = dir-PI/2; // get the starting direction
    force.set(cos(angle),sin(angle)); // compute the maths to get real direction
    force = force * impulse; // multiply rotation by the impulse
    applyForce(force); // call this function to apply force to acceleration
    
    
}

// based on the rotation create a force pvector which holds the correct rotation.
ofVec2f Ship::getDirForce() // direction when accelerate
{
    float angle1 = dir-((PI/2.1)); // get the starting direction
    dir_force.set(cos(angle1),sin(angle1)); // compute the maths to get real direction
    return dir_force;
}

ofVec2f Ship::geForce() // return public force
{
    return force;
}

ofVec2f Ship::getLocation() // return public location
{
    return location;
}
float Ship::getLocationX() // return public x
{
    return location.x;
}
float Ship::getLocationY() // return public y
{
    return location.y;
}
void Ship::setForce(ofVec2f fo) // set force
{
    force  = fo;
}

// apply force
void Ship::applyForce(ofVec2f force) // adding direction to acceleration
{
    
    acceleration = acceleration+force;
    
}

// no force is applied
void Ship::noForce()
{
    acceleration.set(0,0); // if key released dont add any forces
    
}

ofVec2f Ship::getVelociy() // public velocity
{
    return velocity;
}

float Ship:: get_radius() // public radius
{
    return radius;
}

void Ship::setVelocity(ofVec2f ve) // set velocity
{
    velocity = ve;
}

void Ship::setLocation(ofVec2f loc) // set location
{
    location = loc;
}

void Ship::setDir(float d) // set direction
{
    dir = d;
}
float Ship::getDir() // public direction
{
    return dir;
}

ofVec2f Ship::getAcceleration() // get acceleration
{
    return acceleration;
}
void Ship::setAcceleration(ofVec2f ac) // set acceleration
{
    acceleration = ac;
}

// display and rotate the ship
void Ship::display()
{
    ofPushStyle();
    ofSetRectMode(OF_RECTMODE_CENTER);
    ofPushMatrix();
    ofTranslate(location.x, location.y);
    ofRotate(dir*55.1);
    img.draw(0,0); // drawing the ship by center
    // if accelerate display the sprite engine
    if (up ||up1)
    {
        engine[current_image].draw(2, 60);
    }
    ofPopMatrix();
    ofPopStyle();
}


void Ship:: set_time(float t) // ser time
{
    time = t;
    
}

float Ship:: get_time() // public return time
{
    return time;
    
}

void Ship:: set_up1(bool u) // set up_1
{
    up = u;
}




void Ship:: start_acceleration() // animation just before staring the game 
{
    if (time<0)
    {
        up = false;
        time = 3;
    }
    
}


void Ship::keyDown(int i) // using keyPressed inside the class
{
    switch(i)
    {
     case 97: //"a"
            face("left");
     break;
     case 119: //"w"
            getForce();
            face("straight");
            if (ofGetFrameNum()%4 == 0) // slow the sprite down
            {
                current_image = (current_image +1)%6;
            }
            up1 = true;
      break;
      case 100://"d"
      face("right");
      break;
        case 32: // space bar
            if(shot) // if you can shoot
            {
            
            if(id == 0) // check id bullets current use and generate such a bullet
            {
                bl.emplace_back(new Basic_bullet(getLocationX(),getLocationY(),10,7,enem));
            }
            else if(id == 1)
            {
                bl.emplace_back(new Medium_bullet(getLocationX(),getLocationY(),15,10,enem));
            }
            else if(id == 2)
            {
                bl.emplace_back(new Large_bullet(getLocationX(),getLocationY(),25,15,enem));
            }
            
            for(int i = 0;i<bl.size();i++)
                
            {
                
                bl[bl.size()-1]->setSpeed(getDirForce()); // get bullets direction
            }
                shot = false; // used so that the bullet only follows te direction when generated
                
            }
    
            break;
    }
}

void Ship::keyUp(int i) // using keyPressed inside the class
{
    switch(i)
    {
        case 32: //"a"
            shot = true;
            break;
    }
}



bool Ship::get_up() // get up variable
{
    
    return up1;
}

void Ship:: setId(int i) // set id
{
    id = i;
}

int Ship:: getId() // get id
{
    return  id;
}




// using OSC to control the ship movement; same as the keyboard but sending messages to the receiver device
void Ship::OscControl()
{
    if (left)
    {
        face("left");
    }
    if (right)
    {
        face("right");
    }
    if (up)
    {
        getForce();
        face("straight");
        if (ofGetFrameNum()%4 == 0) // play the sprite for the engine if moving
        {
            current_image = (current_image +1)%6;
        }
        
    }
    
    
    while(receive.hasWaitingMessages()) // if any messages are sent
    {
        ofxOscMessage m;
        receive.getNextMessage(&m); // connection between sender and receiver
        
        if (m.getAddress()==("/Rotate_L")) // left button
        {
            
            left = m.getArgAsInt(0);
        }
        
        if (m.getAddress()==("/Rotate_R")) // right button
        {
            right = m.getArgAsInt(0);
        }
        if (m.getAddress()==("/Accelerate")) // accelerate button
        {
           up = m.getArgAsInt(0);
            cout<<"Pressed UP"<<endl;
        }
        if (m.getAddress()==("/Shoot")) // shooting button button
        {
            if(m.getArgAsFloat(0))
            {
                if(id == 0)
                {
                    bl.emplace_back(new Basic_bullet(getLocationX(),getLocationY(),5,7,enem));
                }
                else if(id == 1)
                {
                    bl.emplace_back(new Medium_bullet(getLocationX(),getLocationY(),15,10,enem));
                }
                else if(id == 2)
                {
                    bl.emplace_back(new Large_bullet(getLocationX(),getLocationY(),25,15,enem));
                }

                
                for(int i = 0;i<bl.size();i++)
                    
                {
                    
                    bl[bl.size()-1]->setSpeed(getDirForce());
                }

            }

        }
        if (m.getAddress()==("/accxyz")) // using accelerometer and parameters to smooth the angle of rotation
        {
            angle[counter%amount_smooth] = m.getArgAsFloat(1);
            smooth_angle = 0;
            for (int i = 0;i< amount_smooth; i++)
            {
                smooth_angle = smooth_angle + angle[i];
            }
            smooth_angle = smooth_angle / amount_smooth;
            counter++;
            dir +=smooth_angle *0.5;
            if(dir>=6.55 || dir<=-6.55)
            {
                dir = 0;
            }
            
        }
        
        
        
        
    }
    
}














