//
//  Health_Bar.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 29/02/2016.
//
//

#ifndef Health_Bar_hpp
#define Health_Bar_hpp

#include"ofMain.h"
#include"GameObject.hpp"

// class to keep track of the player health
class Health: public GameObject
{
    
    ofVec2f position;
    float w; // width
    float h; // height
    int r; //red
    int g; //green
    int b; // blue
    int alpha; // aplpha
    int increment; // used to fade the health bar
    ofImage img; // display health image
    
public:
    Health(float _x, float _y, float _w, float _h,int _r,int _g,int _b, int _alpha); // constructor
    virtual ~Health();//disctructor 
    virtual void update(); // display the health
    void load(); // load image
    void set_y(int _y); //public y
    float get_y(); // get y public 
    void set_h(int _h); // public h
    float get_h(); // get h public
    virtual void display(); // dra the health image
    int get_Increment(); // public increment
    void set_increment(int inc); // set increment 
    bool health_out(); // run out of health?
    
    
    
    
    
    
    
    
    
    
    
};

#endif /* Health_Bar_hpp */
