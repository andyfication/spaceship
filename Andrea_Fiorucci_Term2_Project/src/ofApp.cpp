// Andrea Fiorucci
// 23/03/2016
// work in progress term 2 final coursework.
// use UP and DOWN keys to navigate the menu and press ENTER to select.
// use BACK_SPACE to go back to the main menu while playing.
// use 'a' and 'd' to rotate the ship and 'w' to accelerate.
// Supporting OSC controls.
// read the documentation included in the submission for extra informations.



#include "ofApp.h"






//------------------------------------SETUP-----------------------------------------------------------------------
void ofApp::setup(){
    
    // initialise each object with proper constructor
    ship1= new Ship(ofGetWidth()/2,ofGetHeight()-60,3,0.05,"ship1",enemies);
    alien_animation = new Alien(100,ofGetHeight()-50,true);
    target= new Target(100,ofGetHeight()+100,3000,0.2,0.1);
    health_background = new Health(ofGetWidth()-30,250,10,500,100,100,100,100);
    health_left= new Health(ofGetWidth()-30,250,10,500,0,255,0,80);
    collectable= new Score(ofGetWidth()/2-60,200,ship1);
    menu = new Menu(500,400,"Start","How to Play","Back",alien_animation,ship1,health_left,enemies,aliens,collectable);
    
    // pushing objects in a base class vector of type GameObject
     gameobjects.emplace_back(menu);
     gameobjects.emplace_back(ship1);
     gameobjects.emplace_back(alien_animation);
     gameobjects.emplace_back(target);
     gameobjects.emplace_back(health_background);
     gameobjects.emplace_back(health_left);
     gameobjects.emplace_back(collectable);
    
    
    
    
    // loading images for the asteroids
    bas.loadImage("5.png");
    med.loadImage("6.png");
    adv.loadImage("7.png");
    collectable->load_image();
    health_background->load();// load image for the health
    menu->setup_menu_images(); // loading images and text font for the menu
    menu->gameState = "menu"; // game start from the main menu sectino
    isPressed = false; // no key is pressed
    ship1->player_images(); // load images for the player
    alien_animation->alien_loadImages();//load images for animatinos
    isOsc = ship1->get_up(); // used for the OSC controls

    for (int i = 0; i<15;i++) // loading explosion
    {
        ofImage temp;
        string text = "a";
        string text1 = to_string(i);
        temp.loadImage(text+text1+".png");
        explosion.push_back(temp);
        
        
    }
    current_exp = 0; // setting current explosion integer to 0
    
    powers.emplace_back(new Green(200,200,10,ship1)); // display one power up on start up
    

    
    
    
    
    
    
}

//--------------------------------------------------------------
void ofApp::update(){
    gameobjects[0]->update(); // animating the main menu arrows
    menu->randomAsteroids(); // random asteroids generation
    gameobjects[2]->update(); // update the animation
    
    if (isPressed && menu->gameState == "play") // if the state = play then check for key pressed
    {
        ship1->keyDown(currentKey); // call key pressed function
    }
    if (menu->gameState == "play") // if state = play
    {
        menu->update_time(); // menu countdown
        ship1->OscControl(); // control OSC
        //cout<<"hey you are in level 1"<<endl;
        //ship1.start_acceleration();
        gameobjects[1]->update(); // update ship
        random_asteroids(); // genereate asteroids
       
    }
}

//---------------------------------------------------------DRAW-----------------------------------------------------------
void ofApp::draw(){
    
    
    //--------------------------------MAIN MENU STAGE--------------------------------------------------------------------
    if(menu->gameState == "menu")
    {
        display_explosion = false; // no explosion
        display_explosion_moon = false; // no explosion
        if (menu->load_alienVect)
            {
        // aliens in the game
        aliens.emplace_back(new Alien(530,690,true));
        aliens.emplace_back(new Alien(150,820,false));
        aliens.emplace_back(new Alien(340,745,true));
        aliens.emplace_back(new Alien(850,740,false));
        aliens.emplace_back(new Alien(680,800,false));
        aliens.emplace_back(new Alien(990,820,true));
        // load images
        for(auto &al :aliens)
        {
            al->alien_loadImages();
        }
                menu->load_alienVect = false;
                
            }
        if(menu->select()) // first menu section
        {
            menu->displayStart(); // arrows pointing to the start section in main menu
            for(auto &al: aliens) // loop the alien vector
            {
                al->random_direction(); // direction
                al->update(); // update
                al->display(); // display th aliens
            }
            
        }
        else
        {
            menu->displayHow();// arrows pointing to the how to play section of the main menu
            
            for(auto &al: aliens) // repeat the same code
            {
                al->random_direction();
                al->update();
                al->display();
            }
        }
        
        
         collectable->displayHigh(); // display highscore
    }
    //-------------------------- HOW TO PLAY MENU STATE-------------------------------------------------------------------
    
    else if(menu->gameState == "how") // how to play main menu section
    {
        menu->displayBack(); // back display button in how to play section
        menu->instructions(); // display the instructions
        
    }
    else if(menu->gameState == "transaction") // how to play main menu section
    {
        menu->transaction();// menu animation fading up
        
        
    }
    
    else if(menu->gameState == "animation") // how to play main menu section
    {
        
        // display animation with the alien going into the ship
         menu->play();
         gameobjects[1]->display();
         alien_animation->transaction_play();
         menu->animation();
         alien_animation->countDown();
       
        
         
       
        
        
    }
    
   
    //----------------------------- GAME STARTS ---------------------------------------------------------------------------
    
    else if(menu->gameState == "play") // start with the game
    {
        menu->play(); // display game components
        collectable->updateTime();
        collectable->displayTime();
        // loop and update the aliens
        for(auto &al: aliens)
        {
          al->random_direction();
          al->update();
          al->display();
          
        }
        gameobjects[6]->display(); // display collectables
        if(collectable->collected()) // if you collect the stars
        {
            gameobjects[6]->update(); // randomise another one to a different position
        }
        for(int i = 0; i<ship1->bl.size();i++) // looping the bullet vector
            
        {
            ship1->bl[i]->display(); // display them
            ship1->bl[i]->update(); // update them to move towards direction of shot
            if(ship1->bl[i]->collision()) // check collision with steroids
            {
                ofPushStyle();
                ofSetRectMode(OF_RECTMODE_CENTER);
                //display an explosion
                explosion[8].draw(ship1->bl[i]->getPosX(), ship1->bl[i]->getPosY(),200,200);
                ofPopStyle();
                ship1->bl.erase(ship1->bl.begin()+i); // erase the bullet collided
                
            }
            
            
        }
        
        for(int i = 0; i<ship1->bl.size();i++) // loop bullet array
            
        {
            
            if(ship1->bl[i]->outScreen()) // check for bullet out of screen
            {
                ship1->bl.erase(ship1->bl.begin()+i); // erase them
            }
            
        }
        
        // loop and update asteroids
        for(int i = 0; i<enemies.size();i++)
        {
            enemies[i]->display();
            enemies[i]->update();
            enemies[i]->collision_player();
            enemies[i]->collision_moon();
            
            if (enemies[i]->collision_player()) // collision with player
            {
                display_explosion = true; // explosion
                
            }
            if (enemies[i]->collision_moon()) // collision with moon
            {
               
                display_explosion_moon =true; // explosion
                aliens.erase(aliens.begin()); // one alien dies
                float left =alien_animation->get_alien_left();
                left--;
                alien_animation->set_alien_left(left); // update the aliens left variable
            }
            
            if(enemies[i]->destroyed()) // erase the asterid collided with the bullet
            {
                enemies.erase(enemies.begin()+i);
            }
            
        }
        
       
        
        // check if there is a collision with either player or moon and delete the collided asteroids
        for(int i = 0;i<enemies.size();i++)
        {
            if(enemies[i]->collision_player()||enemies[i]->collision_moon())
            {
             enemies.erase(enemies.begin()+i);
            }
        }
        
        gameobjects[3]->update(); // move the target for the advanced asteroid type
        gameobjects[3]->display(); // display the target
        health_background->update(); // display the background of the health bar
        gameobjects[5]->update(); // display the helath bar left
        gameobjects[4]->display(); // draw the image for the health
        
        if(health_left->health_out()||alien_animation->get_alien_left()==0) // run out of health? then game over
        {
            menu->gameState = "lose"; // game_over
        }
        
        gameobjects[1]->display(); // display the player
        explosion_an(); // display animation
        alien_animation->left_alien(); // check for left aliens
        
        
        
        
        for(int i = 0; i<powers.size();i++) // loop power ups vector
        {
            powers[i]->display(); // display them
            powers[i]->time_weapon(); // check time when collected
            if(powers[i]->collision()) // check collision with player for collection
            {
                
                powers.erase(powers.begin()); // erase the power up collected
                // randomise a new power up to display within the screen canvas
                int r = ofRandom(100);
                if(r<50)
                {
                 powers.emplace_back(new Green(ofRandom(100, ofGetWidth()),ofRandom(100, ofGetHeight()),10,ship1));
                }
                else if(r>50)
                {
                 powers.emplace_back(new White(ofRandom(100, ofGetWidth()),ofRandom(100, ofGetHeight()),10,ship1));
                }
                
            }
        }
      
        
        
       
        
    }
    
    //--------------------------- LOSE STAGE-----------------------------------------------------------------------------------------
    
    else if(menu->gameState == "lose")//losing menu section
    {
        menu->displayGameover(); // displaing the losing page
        collectable->checkHigh(); // checking high score
    }
    
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    menu->condition(key); // based on key pressed change the different menu sections
    isPressed = true; // the key is pressed
    currentKey = key; // save key into current key
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    isPressed = false; // key is released
    ship1->up1=false; // ship does not accelerate
    currentKey = key; // save key into current key
    ship1->keyUp(key);
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
}
    //--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 
    
}



void ofApp::random_asteroids() // poliorphism section ( feeding the base class vetor with references to the subclasses)
{
    if(ofGetFrameNum()%200==0) // every 300 frames
    {
        int i = ofRandom(3); // randomise an index
        
        if(i == 0) // if random number = 0 then push basic asteroid into the base class vector
        {
            enemies.emplace_back(new Basic_Asteroid(ofRandom(100, ofGetWidth()),-100,1,bas,15,50,ship1,health_left));
           
           
        }
        else if (i==1) // if random number = 1 then push medium asteroid into the base class vector
        {
            enemies.emplace_back(new Medium_Asteroid(ofRandom(100, ofGetWidth()),-100,2,med,5,15,ship1,health_left));
           
            

        }
        else if(i==2) // if random number = 2 then push advanced asteroid into the base class vector
        {
        
            enemies.emplace_back(new Advanced_Asteroid(ofRandom(100, ofGetWidth()),-100,3,adv,10,40,ship1,target,health_left));
        
            
        }
        
        
    }
    
    
}

void ofApp::explosion_an() // display explosion
{
    if(display_explosion) // player vs asteroids
    {
        
        if (ofGetFrameNum()%4==0)
        {
            current_exp = (current_exp+1)%15;
        }
        
        ofPushStyle();
        ofSetRectMode(OF_RECTMODE_CENTER);
        explosion[current_exp].draw(ship1->getLocationX() ,ship1->getLocationY(),150,150);
        ofPopStyle();
        if (current_exp == 14)
        {
            current_exp = 0;
            display_explosion = false;
        }
    }
    else if(display_explosion_moon) // asteroids vs moon
    {
        
        if (ofGetFrameNum()%4==0)
        {
            current_exp = (current_exp+1)%15;
        }
        
        ofPushStyle();
        ofSetRectMode(OF_RECTMODE_CENTER);
        for(int i = 0; i<5;i++)
        {
            explosion[current_exp].draw(180+(200*i) ,ofGetHeight()-100,200,200);
        }
        ofPopStyle();
        if (current_exp == 14)
        {
            current_exp = 0;
            display_explosion_moon = false;
        }
    }

    
}


