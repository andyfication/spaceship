//
//  Enemy.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 27/02/2016.
//
//

#include "Enemy.hpp"

// constructor takes in position,max speed,image,strength,radius and two references to ship and health classes
Enemy::Enemy(float x,float y,float _max_speed,ofImage _design,int _strenght,int _radius,Ship* _sh,Health* _hl):GameObject(x,y),max_speed(_max_speed),design(_design),strenght(_strenght),radius(_radius),sh(_sh),hl(_hl)
{
    location.x = x;
    location.y = y;
    velocity.set(0,0); // no velocity
    acceleration.set(0, 3); // no acceleration
   // cout<<"Enemy created"<<endl;
    
  
    
}

Enemy::~Enemy() // distructor for base class
{
    //cout<<"Enemy destr"<<endl;
}



void Enemy::update() // update used in the vasic_asteroids
{
    velocity = velocity+acceleration; // add a vertical acceleration to the asteroid
    velocity.limit(max_speed); // limit the velocity
    location = location+velocity; // add it to the location
    
}


bool Enemy:: collision_player() // only for reference
{
    
}

bool Enemy:: collision_moon() // only for reference
{
    
}

float Enemy::getLocationX() // return location x
{
    return location.x;
}

float Enemy::getLocationY() // return location y
{
    return location.y;
}

float Enemy::getRadius() // return radius
{
    return radius;
}

int Enemy::getStrenght() // initialise return strenght
{
    
}

void Enemy::setStrenght(int s) // initialise set strenght
{
    
}

bool Enemy::destroyed() // initialised bool enemy alive
{
    
}



// -----------------------------------------------------------------------------------same parameters as the base class
Basic_Asteroid::Basic_Asteroid(float x,float y,float _max_speed,ofImage _design,int _strenght,int _radius,Ship* _sh,Health* _hl):Enemy(x,y,_max_speed,_design,_strenght,_radius, _sh,_hl)
{
    //cout<<"Basic_Asteroid created"<<endl;
}

Basic_Asteroid::~Basic_Asteroid() // distructor
{
    //cout<<"Basic_Asteroid destr"<<endl;
}

void Basic_Asteroid::display() // displays differently form the base class with a different image
{
    ofPushStyle();
    ofSetRectMode(OF_RECTMODE_CENTER);
    design.draw(location.x, location.y,150,150) ;
    ofPopStyle();
    

}







bool Basic_Asteroid::collision_player() // collides differently with the player as each asteroid have their radius
{

    
    // computing maths to get the distance between th ship and asteroid radius
    float delta_x = sh->getLocationX() - location.x;
    float delta_y = sh->getLocationY() - location.y;
    float distance = sqrt(delta_x * delta_x + delta_y * delta_y);
        if (distance < radius + sh->get_radius()) {
        //cout<<"basic/player/collision"<<endl;
        float temp_y =hl->get_y();
        temp_y+=5;
        hl->set_y(temp_y);
        float temp_h =hl->get_h();
        temp_h-=5;
        hl->set_h(temp_h);
        float temp_i =hl->get_Increment();
        temp_i+=5;
        hl->set_increment(temp_i);
        return true;
    }
        return false;
    
    
}


bool Basic_Asteroid::collision_moon() // collision with moon based on the basic_asteroid radius
{
    if(location.y+radius>ofGetHeight()-100)
    {
        //cout<<"basic/moon/collision"<<endl;
        
        return true;
    }
    
       return false;
  
}

float Basic_Asteroid::getLocationX() // get x
{
    return location.x;
}

float Basic_Asteroid::getLocationY() // get y
{
    return location.y;
}

float Basic_Asteroid::getRadius() // get radius
{
    return radius;
}

int Basic_Asteroid::getStrenght() // get strenght
{
    return strenght;
}

void Basic_Asteroid::setStrenght(int s) // set strenght
{
    strenght = s;
}

bool Basic_Asteroid::destroyed() // return true if strenght is < 0
{
    if(strenght<0)
    {
        return true;
    }
    return false;
}


// -----------------------------------------------------------------------------------------same parameters as the base class
Medium_Asteroid::Medium_Asteroid(float x,float y,float _max_speed,ofImage _design,int _strenght,int _radius,Ship* _sh,Health* _hl):Enemy(x,y,_max_speed,_design,_strenght,_radius, _sh,_hl)
{
    //cout<<"Medium_Asteroid created"<<endl;
}

Medium_Asteroid::~Medium_Asteroid() // distructor
{
    //cout<<"Mdium_Asteroid destr"<<endl;
}

void Medium_Asteroid::display()// displays differently form the base class with a different image

{
    ofPushStyle();
    ofSetRectMode(OF_RECTMODE_CENTER);
    design.draw(location.x, location.y,50,50) ;
    ofPopStyle();
}



void Medium_Asteroid::update() // the update is different for this subclass(the asteroids follow the player)
{
    ofVec2f direction; // direction
    direction = sh->getLocation()-location; // compute difference between player and this asteroid
    direction.normalize(); // normalise to get direction
    direction = direction*0.5; // multiply direction
    acceleration = direction; // set it as the acceleration
    velocity = velocity+acceleration; // velocity +acceleration
    velocity.limit(max_speed); // limit the velocity
    location = location+velocity; // add it to location
    
    
    
}



bool Medium_Asteroid::collision_player() // collides differently with the player as each asteroid have their radius
{
    // maths computed for the collision
    float delta_x = sh->getLocationX() - location.x;
    float delta_y = sh->getLocationY() - location.y;
    float distance = sqrt(delta_x * delta_x + delta_y * delta_y);
    
    if (distance < radius + sh->get_radius()) {
        
        //cout<<"medium/player/collision"<<endl;
        float temp_y =hl->get_y();
        temp_y+=15;
        hl->set_y(temp_y);
        float temp_h =hl->get_h();
        temp_h-=15;
        hl->set_h(temp_h);
        return true;
        float temp_i =hl->get_Increment();
        temp_i+=15;
        hl->set_increment(temp_i);
    }
    return false;
}

bool Medium_Asteroid::collision_moon() // collision with the monn based on the medium_asteroid radius
{
    if(location.y+radius>ofGetHeight()-100)
    {
        //cout<<"medium/moon/collision"<<endl;
        return true;
    }
    
    return false;

}

float Medium_Asteroid::getLocationX() // get x
{
    return location.x;
}

float Medium_Asteroid::getLocationY() // get y
{
    return location.y;
}

float Medium_Asteroid::getRadius() // get radius
{
    return radius;
}

int Medium_Asteroid::getStrenght() // get strenght
{
    return strenght;
}

void Medium_Asteroid::setStrenght(int s) // set strenght
{
    strenght = s;
}

bool Medium_Asteroid::destroyed() // return true if strenght <0
{
    if(strenght<0)
    {
        return true;
    }
    return false;
}

// same parameters as the base class and added a memner variable of this subclass which s a reference to the target
Advanced_Asteroid::Advanced_Asteroid(float x,float y,float _max_speed,ofImage _design,int _strenght,int _radius,Ship* _sh,Target* _target,Health* _hl):Enemy(x,y,_max_speed,_design,_strenght,_radius,_sh,_hl),target(_target)
{
    //cout<<"Advanced_Asteroid created"<<endl;
}

Advanced_Asteroid::~Advanced_Asteroid() // distructor
{
    //cout<<"Advanced_Asteroid destr"<<endl;
}
void Advanced_Asteroid::display() // display advanced_asteroid differently
{
    ofPushStyle();
    ofSetRectMode(OF_RECTMODE_CENTER);
    design.draw(location.x, location.y,100,100) ;
    ofPopStyle();
}

void Advanced_Asteroid::update() // update is different (the advanced_asteroid moves sideways since it is following an invisible target while moving down)
{
    ofVec2f dir; // direction
    dir = target->get_position()-location; // difference between this asteroid and target
    dir.normalize(); // normalise
    dir = dir*0.5; // multiply
    acceleration = dir; // acceleration = direction
    velocity = velocity+acceleration; // add acceleration to velocity
    velocity.limit(max_speed); // limit velocity
    location = location+velocity; // add velocity to the location
    
    
    
}



bool Advanced_Asteroid::collision_player() // collides differently with the player as each asteroid have their radius
{
    //maths used to compute the collision between two radius
    float delta_x = sh->getLocationX() - location.x;
    float delta_y = sh->getLocationY() - location.y;
    float distance = sqrt(delta_x * delta_x + delta_y * delta_y);
    
    if (distance < radius + sh->get_radius()) {
       
        cout<<"advanced/player/collision"<<endl;
        float temp_y =hl->get_y();
        temp_y+=25;
        hl->set_y(temp_y);
        float temp_h =hl->get_h();
        temp_h-=25;
        hl->set_h(temp_h);
        float temp_i =hl->get_Increment();
        temp_i+=25;
        hl->set_increment(temp_i);
        return true;
    }
    return false;
}

bool Advanced_Asteroid::collision_moon()// collision with the monn based on the advanced_asteroid radius
{
    if(location.y+radius>ofGetHeight()-100)
    {
        //cout<<"advanced/moon/collision"<<endl;
        return true;
    }
    
    return false;
 
}

float Advanced_Asteroid::getLocationX() // get x
{
    return location.x;
}

float Advanced_Asteroid::getLocationY() // get y
{
    return location.y;
}
float Advanced_Asteroid::getRadius() // get radius
{
    return radius;
}
int Advanced_Asteroid::getStrenght() // get strenght
{
    return strenght;
}

void Advanced_Asteroid::setStrenght(int s) // set strenght
{
    strenght = s;
}
bool Advanced_Asteroid::destroyed() // return true if strenght < 0
{
    if(strenght<0)
    {
        return true;
    }
    return false;
}



