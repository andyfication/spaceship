//
//  Aliens.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 12/02/2016.
//
//

#include "Aliens.hpp"
// constructor takes in position, and direction of the alien
Alien::Alien(float _x, float _y,bool _rand):GameObject(_x,_y),rand(_rand)
{
    current_alien_l = 0;
    current_alien_r = 6;
    see = true;
    active = false;
    max = 6;
    aliens_left = 6;
    position.x = _x;
    position.y = _y;
    
}

Alien::~Alien()//disctructor 
{
    
}


void Alien::alien_loadImages() // looping and pushing images in the vector
{
    for (int i = 0; i<11;i++)
    {
        ofImage temp1;
        string text = to_string(0);
        string text2 = to_string(i);
        temp1.loadImage(text+text2+".png");
        alie.push_back(temp1);
        
    }
    start.loadFont("font.tff", 30);
    start1.loadFont("font.tff", 150);
    start2.loadFont("font.tff", 150);
    start3.loadFont("font.tff", 15);
    
    
}


bool Alien:: random_direction() // randomise a boolean left and righ (not a real random just switching)
{
    if (ofGetFrameNum()%110==0)
    {
        rand = !rand;
    }
}


void Alien::display() // display alien
{
   
    
    if(rand) // if moving right
    {
    alie[current_alien_r].draw(position.x,position.y ); // display right
    position.x++;
    }
    else { // display left 
    alie[current_alien_l].draw(position.x,position.y );
        position.x--;
    }
    
    
}

void Alien:: left_alien() // animation alien
{
    string t = to_string(aliens_left);
    alie[0].draw(10,10,70,60);
    start3.drawString("  X" , 90,60);
    start.drawString(t, 125,60);
}

int Alien::getMax() // get max
{
    
    return max;
}

void Alien:: setMax(int m) // set max
{
    max = m;
}

void Alien::update() // update aliens by looping the sprite
{
    
    if (ofGetFrameNum()%10 == 0) // slow the sprite down
    {
        current_alien_r = (current_alien_r +1)%5;
        if(current_alien_l>8)
        {
            current_alien_l = 6;
        }
        current_alien_l =(current_alien_l+1);

    }

}

bool Alien::getSee() // public see
{
    return see;
}

int Alien:: get_alien_left()
{
    return aliens_left;
}

void Alien:: set_alien_left(int a)
{
    aliens_left = a;
}

void Alien::setSee(bool s) // set s
{
    see = s;
}

void Alien :: countDown() // start the countdown
{
    if(active) // if active
    {
   if (ofGetFrameNum()%50==0 && max>-2)
   {
       max--; // countdown begins
       
   }
    string temp =to_string(max);
        if(max>0) // if more than 0
        {
        if(max==10) //== 10
        {
         start1.drawString(temp, ofGetWidth()/2-90,ofGetHeight()/2); // draw numbers
        }
        else{
        start1.drawString(temp, ofGetWidth()/2-50,ofGetHeight()/2);
        }
            
        }
      if(max==0)// display "go"
      {
          start2.drawString("GO!", ofGetWidth()/2-200,ofGetHeight()/2);
      }
       
        
       
    }
    
}

bool Alien::getActive() // get active
{
    return active;
}

void Alien::setActive(bool act) // set active
{
    active = act;
}

float Alien::getX() // get x
{
    return position.x;
}

void Alien::setX(float _x) // set x
{
    position.x= _x;;
}
float Alien::getY() // get y
{
    return position.y;
}

void Alien::setY(float _y) // set y
{
    position.y= _y;;
}


void Alien::transaction_play() // perform transaction in the menu
{
    if (active) // if active
    {
  
    if(see) // you can see the aline animation
    {
    alie[10].draw(ofGetWidth()/2-60, ofGetHeight()-70,70,70);
  
    alie[current_alien_r].draw(position.x, position.y);
    }
    position.x+=2; // move the guy left
    if(position.x>510) // move the guy up (entering the ship)
    {
        position.y--;
        position.x--;
    }
    
    if(position.x>510&&position.y<800)
    {
        see =false;
        position.x = 100;
        position.y = ofGetHeight()-50;
    }
    }
    else{ // no more active ,then display press a key to start the adventure 
        
       
        ofPushStyle();
        ofSetColor(ofRandom(100), ofRandom(100), 0);
        ofFill();
        start.drawString("Press any key to start the adventure", 240, 400);
         ofPopStyle();
       
        }
    
    
}







