//
//  Ship.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 09/02/2016.
//
//

#ifndef Ship_hpp
#define Ship_hpp

#include "ofMain.h"
#include"ofxOsc.h"
#include"Ship_Confing.hpp"
#include"Bullets.hpp"
#include"GameObject.hpp"


class Enemy;


#define PORT 12345

// class which defines my player Ship
// Some of the customisable values are set in the Ship_Confing class (limit,impulse and design)
class Ship: public GameObject
{
public :
    vector <unique_ptr<Bullet>> bl;
    
private:
    ofVec2f location; // player location
    ofVec2f velocity; // player velocity (speed)
    ofVec2f acceleration; // player acceleration which will change the velocity/speed
    ofVec2f force; // force changes the acceleratino based on the ship rotation
    ofVec2f dir_force; // used for the bullets
    ofxOscReceiver receive; // set up a receiver in order to use TOUCH_OSC to control the game another device
    float dir; // keeps track of rotation applied to the player when the ship rotates
    bool right; // boolean rotate right
    bool left; // boolean rotate left
    bool up; // boolean move
   
    vector <float> angle; // float of a collcetion of angles which helps to make the phone Accelerometer easier.
    int counter; // keeps track og the current angle
    int amount_smooth; // how smooth do we want the rotation
    float smooth_angle; // value added to the rotation to make it smooth
    vector <ofImage> engine; // vector of images to draw the ship engine
    int current_image; // which image is displayed now
    ofImage img; // player image
    float limit; // limit the velocity
    float impulse; // impulse
    float time; // used for starting impulse animation
    Ship_Config* conf;  // only need one configuration for my ship since there is only one player
    string label;
    float radius; // radius for collision
    int id;
    bool shot;
    vector<unique_ptr<Enemy>>& enem;

    
public:
    
    Ship(float _x, float _y,float lim,float imp,string a, vector<unique_ptr<Enemy>>& _enem); // constructor to set the x and y position,max velocity,impulse and reference to Enemy
    
    Ship(const Ship& first); // copy constructor
    
    Ship& operator=(const Ship& first);// Overloaded assigment
    
     virtual ~Ship(); // distructor
    
    virtual void update(); // update the ship
    void face(string direction); // ship facing direction
    void getForce();// get the rotating force of the ship
    ofVec2f getDirForce();
    ofVec2f geForce(); // public force
    void setForce(ofVec2f fo); // set force
    ofVec2f getVelociy(); //public velocity
    void setVelocity(ofVec2f ve); // set velocity
    void applyForce(ofVec2f force); // apply the force to the acceleration
    void noForce(); // no force applied to the acceleration
    virtual void display(); // display the ship
    void keyDown(int i);// key pressed to control the ship
    void keyUp(int i);// key pressed to control the ship

    void OscControl(); // OSC for TOUCH-OSC to control the ship with a device
    void player_images(); // load images for player
    bool get_up(); // public up
    bool set_up(bool s); // set up
    bool up1; // boolean move for TOUCH_OSC
    void setLocation(ofVec2f loc); // set location
    float getDir(); // public direction
    void setDir(float d); // set direction
    ofVec2f getAcceleration(); // public acceleration
    ofVec2f getLocation(); // public location
    float getLocationX(); // public x
    float getLocationY(); //public y
    void setAcceleration(ofVec2f ac); //set acceleration
    void start_acceleration(); // animation impluse before starting the level
    void set_time(float t); // set time
    float get_time(); // public time
    void set_up1(bool u); // set up1
    float get_radius(); // public radius
    void setId(int i); // set id for bullets
    void setShot(bool s); // boolean for shooting
    int getId(); // get id for bullets
    
    
    
    
    
};


#endif /* Ship_hpp */
