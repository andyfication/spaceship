//
//  Power.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 07/03/2016.
//
//

#include "Power.hpp"

// takes in the position,radius,reference to ship,
Power::Power(float _x, float _y, float _radius,Ship* _ship):GameObject(_x,_y),radius(_radius),ship(_ship)
{
    position.x =_x;
    position.y = _y;
    time = 500; // time for the power up last
    s = false; // animate power up
    sc = 5;
    
}

Power::~Power()//disctructor
{
    
}

void Power:: display() // display the power up and animate it
{
    
    ofPushStyle();
    ofSetColor(255, 100, 100);
    ofFill();
    ofCircle(position.x,position.y,radius);
    ofPopStyle();
}

void Power:: update() // randomise the power up
{
    position.x = ofRandom(100, ofGetWidth()-100);
    position.y = ofRandom(100, ofGetHeight()-300);
    
}

bool Power:: collision() // collision initialisation
{
    
    
}

void Power::time_weapon() // how long does the power up last
{
    if (ship->getId() !=0) // if not base weapon the time starts ticking
    {
        
        time--;
        //cout<<time<<endl;
        if(time<0) // if not time left the weapon is set to base one
        {
            ship->setId(0);
            time = 500;
        }
    }
}




// --------------------------------------------------------------------------------------white collectables
White::White(float _x, float _y, float _radius,Ship* _ship):Power(_x,_y,_radius,_ship)
{ }

White::~White()//disctructor
{
    
}

void White:: display() // display for white power up(change the radius)
{
  // animating it by scaling the objetc
     if(sc>20)
        {
            s = true;
        }
        else if(sc<0)
        {
            s = false;
        }
        
        if(s)
        {
            sc-=0.5;
        }
        else
            sc+=0.5;
    ofPushStyle();
    ofSetColor(255,255,255,100);
    ofFill();
    ofCircle(position.x,position.y,sc); // display animated power up
    ofPopStyle();
}

bool White:: collision() // changes radiuos collision
{
    if(ofDist(position.x, position.y, ship->getLocationX(), ship->getLocationY())<ship->get_radius())
    {
        ship->setId(1); // set current weapon id
        return true;
    }
    return false;
}


// --------------------------------------------------------------------------------------green collectables

Green::Green(float _x, float _y, float _radius,Ship* _ship):Power(_x,_y,_radius,_ship)
{ }


Green::~Green()//disctructor 
{
    
}


void Green:: display() // display the green(changes radius)
{
    // animating it by scaling the objetc
    if(sc>20)
    {
        s = true;
    }
    else if(sc<0)
    {
        s = false;
    }
    
    if(s)
    {
        sc-=0.5;
    }
    else
        sc+=0.5;
    ofPushStyle();
    ofSetColor(0,255,0,100);
    ofFill();
    ofCircle(position.x,position.y,sc); // display animated power up

    ofPopStyle();
}

bool Green:: collision() // collision changes the radius 
{
    if(ofDist(position.x, position.y, ship->getLocationX(), ship->getLocationY())<ship->get_radius())
    {
        ship->setId(2); // set current weapon id

        return true;
    }
    return false;
}