//
//  Ship_Confing.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 09/02/2016.
//
//

#ifndef Ship_Confing_hpp
#define Ship_Confing_hpp

#include "ofMain.h"

// Player ship configuration. Set the speed limit for the ship, the design and the impulse when accelerating.
class Ship_Config
{
    float lim; // speed max limit
    float imp; // acceleration from 0 to max speed
    ofImage ship_player; // image to draw the ship design
    
    
public:
    Ship_Config(float _lim = 3, float _imp = 0.05); // explicit default constructor
    void set_image_ship(); // ship design
    float get_Limit(); // get a public limit value
    float get_Impulse(); // get a public impulse value
    ofImage get_Image(); // get a public ship image
    
    
    
    
    
    
    
    
};

#endif /* Ship_Confing_hpp */
