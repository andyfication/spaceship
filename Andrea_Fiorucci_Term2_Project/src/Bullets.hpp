//
//  Bullets.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 07/03/2016.
//
//

#ifndef Bullets_hpp
#define Bullets_hpp

#include <stdio.h>
#include"ofMain.h"
#include"GameObject.hpp"

class Enemy; // forward declaration as I am already including bullet in enemy


// class bullets used for the shooting part
class Bullet:public GameObject
{

protected:
    ofVec2f position; // position
    ofVec2f speed; // speed
    int fire_speed; // fire speed
    float radius; // radius of the bullet
    vector<unique_ptr<Enemy>>& enems; // refernece to a unique pointer base class Enemy
    int l_bas; //temporary for basic bullets to take out the asteroid life
    int l_med;//temporary for mediumbullets to take out the asteroid life
    int l_adv;//temporary for advanced bullets to take out the asteroid life
    
    
    
    

public:
    // constructor
    Bullet(float tx, float ty,float _radius,float _fire_speed,vector<unique_ptr<Enemy>>& _enems);
    virtual ~Bullet();//disctructor
    virtual void display(); // display
    virtual void update(); // move/update bullet direction
    void setSpeed(ofVec2f sp); // set bullet speed
    virtual bool collision(); // check collision
    bool outScreen(); // check for out of screen collision
    float getPosX(); // get position x
    float getPosY(); // get position y
    
};



// --------------------------------------------------------------------------------------------basic bullet
class Basic_bullet: public Bullet // basic bullets with a different radius
{
    
    
public:
    // constructor
    Basic_bullet(float tx, float ty,float _radius,float _fire_speed,vector<unique_ptr<Enemy>>& _enems);
    virtual ~Basic_bullet();//disctructor
    virtual void display(); // display differently
    virtual void update(); // move/update differently
    virtual bool collision(); // check collision differently
    
};

// ---------------------------------------------------------------------------------------------advanced bullet
class Medium_bullet: public Bullet
{
    
    
public:
    // constructor
    Medium_bullet(float tx, float ty,float _radius,float _fire_speed,vector<unique_ptr<Enemy>>& _enems);
    virtual ~Medium_bullet();//disctructor
    virtual void display(); // display differently
    virtual void update();  // update differently
    virtual bool collision(); // check collision differently

    
};

// -----------------------------------------------------------------------------------------------controlled bullets
class Large_bullet: public Bullet
{
    
    
public:
    // constructor
    Large_bullet(float tx, float ty,float _radius,float _fire_speed,vector<unique_ptr<Enemy>>& _enems);
    virtual ~Large_bullet();//disctructor 
    virtual void display(); // display differently
     virtual void update();  // update differently
    virtual bool collision(); // check collision differently

    
};

#endif /* Bullets_hpp */
