//
//  Power.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 07/03/2016.
//
//

#ifndef Power_hpp
#define Power_hpp

#include <stdio.h>
#include"ofMain.h"
#include "Ship.hpp"
#include"GameObject.hpp"

// class power up used to collect different weapons
class Power:public GameObject
{
protected:
    ofVec2f position; // position of the power up
    float radius; // radius of the collectable
    int time; // used to hold the current power up for a limited time
    bool s; // scaling animation
    float sc; // max and minimum animation rafius for the collectables
    Ship* ship; // reference to ship

public:
    Power(float _x, float _y, float _radius,Ship* _ship); // constructor
    virtual~Power(); //disctructor
    virtual void display(); // display the power up
    virtual void update(); // randomise the power up
    virtual bool collision(); // check collision with player
    void time_weapon(); // hold the power up for a certain amount of time
    
  
    
};

//-----------------------------------------------------------------
class White: public Power // white power up
{

public:
    White(float _x, float _y, float _radius,Ship* _ship); // same constructor
    virtual ~White();//disctructor
    virtual void display(); // different display
    virtual bool collision(); // different collision based on the radius
    
};

//-----------------------------------------------------------------
class Green: public Power
{

public:
    Green(float _x, float _y, float _radius,Ship* _ship); // same constructor
    virtual ~Green();//disctructor 
    virtual void display(); // different display
    virtual bool collision(); // different collision based on the radius 
    
    
    
};

#endif /* Power_hpp */
