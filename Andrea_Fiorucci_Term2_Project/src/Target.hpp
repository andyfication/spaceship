//
//  Target.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 27/02/2016.
//
//

#ifndef Target_hpp
#define Target_hpp

#include <stdio.h>
#include"ofMain.h"
#include"GameObject.hpp"

// class target used to compute the Advanced_Asteroid movements
class Target:public GameObject
{
    ofVec2f position; // position
    float amplitude; // amplitude
    float frequency; // frequency
    float t; // time
    float dt; // delta_time
    
public:
    
Target(float x, float y,float amp, float freq, float delta); // constructor
virtual ~Target();//disctructor 
virtual void display(); // display the target
virtual void update(); // move target
ofVec2f get_position(); // public position
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
};

#endif /* Target_hpp */
