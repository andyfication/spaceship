//
//  Score.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 04/03/2016.
//
//

#include "Score.hpp"

// constructor takes position, anf reference to the ship
Score::Score (float _x, float _y,Ship* _sh):GameObject(_x,_y),sh(_sh)
{
    sx = 30; // scaler
    score = 0; // score to 0
    scale = false; // switch to aimate the score
    highscore = 0; // no highscore on start up
    reset= false; // not resetting the time
    position.x = _x;
    position.y = _y;
}

Score::~Score()//disctructor 
{
    
}

void Score:: load_image() // load images
{
    im.loadImage("score.png");
    sc.loadFont("font.tff", 15);
    sc1.loadFont("font.tff", 30);
}


void Score:: display() // display the collectables and animate them with a scaler
{
    ofPushStyle();
    ofSetRectMode(OF_RECTMODE_CENTER);
    im.draw(position.x,position.y,sx,sx);
    ofPopStyle();
    if(sx>40)
    {
        scale = true;
    }
    else if(sx<30)
    {
        scale = false;
    }
    
    if(scale)
    {
        sx--;
    }
    else
        sx++;
    
    string temo = to_string(score);
    sc.drawString("X  ", ofGetWidth()-80, 55);
    sc1.drawString(temo, ofGetWidth()-50, 55);
    im.draw(ofGetWidth()-130,15,40,40);
    
    
}

void Score::updateTime() // reset and strat time when play the level
{
    if (reset)
    {
     ofResetElapsedTimeCounter();
        milliseconds = 0;
        seconds = 0;
        minutes = 0;
        
        reset = false;
    }
    milliseconds = ofGetElapsedTimeMillis();
    seconds = floor(milliseconds/1000);
    minutes = floor(seconds/60);
    seconds = seconds%60;
  
    
    
}

void Score:: setReset(bool r) // set reset boolean variable
{
    reset = r;
}


void Score:: checkHigh() // check highscore
{
    if(score>highscore)
    {
        highscore = score;
    }
    
    if (milliseconds>high_millis) // check for highmillis
    {
        high_millis = milliseconds;
    }
}
void Score:: setMinutes(int m) // set minutes
{
    minutes = m;
}
void Score:: setSeconds(int s) // set seconds
{
    seconds = s;
}

int Score:: getMinutes() // get minutes
{
    return minutes;
}
int Score:: getSeconds() // get seconds
{
    return seconds;
}


void Score:: displayTime() // display the time using a string
{
    ofPushStyle();
    ofSetColor(255, 255, 255,150);
    ofFill();
    string temp = "Time   m: " +to_string(minutes)+"  s: " +to_string(seconds);
    sc1.drawString(temp, 445, 60);
    ofPopStyle();
}

void Score::displayHigh() // display the high score in the main screen
{
    ofPushStyle();
    ofSetColor(255, 180, 100,100);
    ofFill();
    string temp = to_string(highscore);
    sc1.drawString("HighScore:  "+temp, ofGetWidth()/2-125, 622);
    ofPopStyle();
    }
bool Score:: collected() // check for collisio nand collection between player and score collectables
{
    if(ofDist(position.x, position.y, sh->getLocationX(), sh->getLocationY())<sh->get_radius())
    {
        score++;
        return true;
    }
    return false;
}
int Score::getScore() // get score
{
    return score;
}

int Score:: getHigh() // get highscore
{
    return highscore;
}

void Score:: setHigh(int h) // set highscore
{
    highscore = h;
}
void Score::setScore(int s) // set score
{
    score = s;
}

void Score:: update()
{
   position.x = ofRandom(100, ofGetWidth()-100);
   position.y = ofRandom(100, ofGetHeight()-200);
}