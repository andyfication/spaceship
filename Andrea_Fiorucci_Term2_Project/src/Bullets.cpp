//
//  Bullets.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 07/03/2016.
//
//

#include "Bullets.hpp"
#include"Enemy.hpp"

// constructor takes in position, radius, speed and a reference to a unique base class pointer enemy
Bullet:: Bullet (float tx, float ty ,float _radius, float _fire_speed,vector<unique_ptr<Enemy>>& _enems):GameObject(tx,ty),enems(_enems)
{
    position.x = tx;
    position.y = ty;
    speed.set(0,0);
    radius = _radius;
    fire_speed = _fire_speed;
   
}

Bullet::~Bullet()//disctructor
{
    
}

float Bullet:: getPosX() // get x
{
    return position.x;
}
float Bullet:: getPosY() // get y
{
    return position.y;
}
void Bullet:: display() // display initialise
{
    
}

void Bullet:: update() // initialise move
{
    
}

void Bullet::setSpeed(ofVec2f sp) //set speed
{
    speed = sp;
}

bool Bullet::collision() // initilise collision
{
    
}

bool Bullet:: outScreen() // check if bullets are out of screen
{
    if(position.x>ofGetWidth()||position.x<0||position.y<0||position.y>ofGetHeight())
    {
        return true;
    }
}


// --------------------------------------------------------------------------constructor same as base class for basic_bullet
Basic_bullet:: Basic_bullet (float tx, float ty ,float _radius, float _fire_speed,vector<unique_ptr<Enemy>>& _enems):Bullet(tx,ty,_radius,_fire_speed,_enems)
{

}

Basic_bullet::~Basic_bullet()//disctructor
{
    
}


void Basic_bullet:: display() // display with a fill and radius
{
    ofPushStyle();
    ofSetColor(255,0,0,150);
    ofFill();
    ofCircle(position.x, position.y, radius);
    ofPopStyle();
}

void Basic_bullet:: update() // update the bullet to get shot in the player direction
{
    
    position+= speed*fire_speed;
    
    
}

bool Basic_bullet::collision() // check collision with asteroids
{
    
    for (int i = 0;i<enems.size();i++) // loop asteroids vecotr
    {
        
        
        float delta_x = enems[i]->getLocationX() - position.x;
        float delta_y = enems[i]->getLocationY() - position.y;
        float distance = sqrt(delta_x * delta_x + delta_y * delta_y);
        if (distance < radius + enems[i]->getRadius()) // if there is a collision
        {
            l_bas = enems[i]->getStrenght();
            l_bas--; // reduce the asteroid stenght
            enems[i]->setStrenght(l_bas);
            cout<<enems[i]->getStrenght()<<endl;

            

            return true;
        }
    }
    
    
    
}


// --------------------------------------------------------------------------------constructor same as base class for advanced_bullet
Medium_bullet:: Medium_bullet (float tx, float ty ,float _radius, float _fire_speed,vector<unique_ptr<Enemy>>& _enems):Bullet(tx,ty,_radius,_fire_speed,_enems)
{
   
}

Medium_bullet::~Medium_bullet()//disctructor
{
    
}


void Medium_bullet:: display()  // display with a fill and radius

{
    ofPushStyle();
    ofSetColor(255,255,255,100);
    ofFill();
    ofCircle(position.x, position.y, radius);
    ofPopStyle();
}

void Medium_bullet:: update() // update the bullet to get shot in the player direction
{
    
    position+= speed*fire_speed;
    
    
}

bool Medium_bullet::collision()  // check collision with asteroids

{
    for (int i = 0;i<enems.size();i++)
    {
        float delta_x = enems[i]->getLocationX() - position.x;
        float delta_y = enems[i]->getLocationY() - position.y;
        float distance = sqrt(delta_x * delta_x + delta_y * delta_y);
        if (distance < radius + enems[i]->getRadius()) // if there is a collision
        {
            l_med = enems[i]->getStrenght();
            l_med-=5; // reduce the asteroid strenght
            enems[i]->setStrenght(l_med);
            cout<<enems[i]->getStrenght()<<endl;
            return true;

        }
    }
}

// ------------------------------------------------------------------------------------------constructor same as base class for advanced_bullet

Large_bullet:: Large_bullet (float tx, float ty ,float _radius, float _fire_speed,vector<unique_ptr<Enemy>>& _enems):Bullet(tx,ty,_radius,_fire_speed,_enems)
{
    
}

Large_bullet::~Large_bullet() //disctructor
{
    
}


void Large_bullet:: display() // display with a fill and radius
{
    ofPushStyle();
    ofSetColor(0,255,0,100);
    ofFill();
    ofCircle(position.x, position.y, radius);
    ofPopStyle();
}

void Large_bullet:: update()  // update the bullet to get shot in the player direction

{
    
    position+=speed*fire_speed;
    
    
}

bool Large_bullet::collision() // check collision with asteroids
{
    for (int i = 0;i<enems.size();i++)
    {
    
        float delta_x = enems[i]->getLocationX() - position.x;
        float delta_y = enems[i]->getLocationY() - position.y;
        float distance = sqrt(delta_x * delta_x + delta_y * delta_y);
        if (distance < radius + enems[i]->getRadius()) // if there is a collision
        {
            l_adv = enems[i]->getStrenght();
            l_adv-=10; // reduce the asteroid strenght 
            enems[i]->setStrenght(l_adv);
            cout<<enems[i]->getStrenght()<<endl;
            return true;

        }
    }
}




