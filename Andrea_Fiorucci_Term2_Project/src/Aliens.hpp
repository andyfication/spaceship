//
//  Aliens.hpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 12/02/2016.
//
//

#ifndef Aliens_hpp
#define Aliens_hpp
#include "ofMain.h"
#include "GameObject.hpp"
// class Alien used for initial animations and display the little aliens in the game
class Alien:public GameObject
{
    ofVec2f position;
    vector<ofImage> alie; // array of images
    int direction; // direction
    int current_alien_l; // current image display left
    int current_alien_r; // current image display right
    int max; // countdown value
    bool see;
    bool active;
    bool rand; // left and right movements
    ofTrueTypeFont start; // font
    ofTrueTypeFont start1; // font
    ofTrueTypeFont start2; // font
    ofTrueTypeFont start3; // font
    int aliens_left; // how many aliens left 
    
    
public:
    int getMax(); // public max
    void setMax(int m); // set max
    Alien(float _x, float _y,bool _rand); // constructor
    virtual ~Alien();//disctructor 
    virtual void update(); // update function
    virtual void display(); // display the aliens
    void transaction_play(); // play a transaction in the main menu
    void alien_loadImages(); // load images
    bool getActive(); // public active
    void setActive(bool act); // set active
    void countDown(); // function for the countdown animatino
    bool getSee(); // public see boolean
    void setSee(bool s); // set see boolean
    float getX(); // public x
    float getY(); // public y
    void setX(float _x); // set x
    void setY(float _y); // set y
    bool random_direction(); // calculate random direction
    void left_alien(); // number of aliens alive
    int get_alien_left(); // get public number 
    void set_alien_left (int a); // reset back the number of aliens alive
   
    
    
    
    
    
    
    
};

#endif /* Aliens_hpp */
