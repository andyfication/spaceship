//
//  Ship_Confing.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 09/02/2016.
//
//

#include "Ship_Confing.hpp"

Ship_Config::Ship_Config(float _limit,float _impulse):lim(_limit),imp(_impulse)
{
    
    
}

void  Ship_Config::set_image_ship()
{
    ship_player.load("ship.png");
    
}

ofImage Ship_Config::get_Image()
{
    
    return ship_player;
}

float Ship_Config::get_Limit()
{
    return lim;
}

float Ship_Config::get_Impulse()
{
    return imp;
}