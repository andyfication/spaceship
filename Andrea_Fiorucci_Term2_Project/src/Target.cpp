//
//  Target.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 27/02/2016.
//
//

#include "Target.hpp"

// takes in position,amplitude,frequency,delta_time
Target::Target(float x, float y,float amp, float freq, float delta):GameObject(x,y),amplitude(amp),frequency(freq),dt(delta)
{
    position.set(x,y);
    
}

Target::~Target()//disctructor 
{
    
}


void Target::update() // move the target using a sine wave for the x coordinate
{
    position.x = ofGetWidth()/2 + amplitude*sin(frequency*(t+dt)); // setting the enemy at a position and animate it
    t+=dt;
}

// display the target
void Target::display()
{
    ofPushStyle;
    ofSetColor(255);
    ofFill();
    ofCircle(position.x,position.y,20);
    
}



ofVec2f Target :: get_position() // return public position
{
    
    return position;
}
