//
//  GameObject.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 14/03/2016.
//
//

#include "GameObject.hpp"

// constructor takes in the object position using a vector
GameObject::GameObject(float _x, float _y)
{
    position.x = _x;
    position.y = _y;
}

GameObject::~GameObject() // distructor 
{
    
}

void GameObject::display() // only initialise display but not using it in the base class 
{
    
}

void GameObject::update() // only initialise update function but not using it in the base class 
{
    
}