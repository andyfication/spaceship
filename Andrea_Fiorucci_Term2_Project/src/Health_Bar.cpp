//
//  Health_Bar.cpp
//  Andrea_Fiorucci_Term2_Project
//
//  Created by Andrea Fiorucci on 29/02/2016.
//
//

#include "Health_Bar.hpp"

// take is position, dimension and colour
Health::Health(float _x, float _y, float _w, float _h,int _r,int _g,int _b, int _alpha):GameObject(_x,_y),w(_w),h(_h),r(_r),g(_g),b(_b),alpha(_alpha)
{
  increment = 0; // no increment
    position.x = _x;
    position.y = _y;
    
}

Health::~Health()//disctructor 
{
    
}


void Health::load() // load image
{
    img.load("ship1.png");
}

void Health:: update() // display the rectangle and fade to red as running out of health
{
    ofPushStyle();
    ofSetColor(r+increment,g-increment,b,alpha);
    ofFill();
    ofRect(position.x,position.y,w,h);
    ofPopStyle();
    
    
    
}

void Health::display() // draw the image which correspond to the health
{
    ofPushStyle();
    ofSetColor(255, 255, 255,150);
    ofFill();
    img.draw(position.x-12, position.y+h+5);
    ofPopStyle();
}


void Health::set_y(int _y){ // set y
    
    position.y = _y;
}

float Health::get_y() // get y
{
    return position.y;
}

void Health::set_h(int _h){ // set h
    
    h = _h;
}

float Health::get_h() // get h
{
    return h;
}

void Health::set_increment(int inc) // set increment
{
    
    increment   = inc;
}

int Health::get_Increment() // get increment
{
    return increment;
}

bool Health::health_out() // when the health is 0 (mechanics made with two rectangles) then return true
{
    if(position.x>position.x+h)
    {
    return true;
    }
    return false;
}